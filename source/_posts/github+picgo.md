---
title: github图床+picgo+vscode
date: 2024/2/7 15:30:25 # 文章发表时间
tags:
  - github
  - picgo
categories: 博客 # 分类
cover: https://cdn.jsdelivr.net/gh/jacktian151/pic@main/github+picgo-2024-02-27-16-09-14.png # 略缩图
---

> 记录一下使用[github](https://github.com/)当图床+[picgo](https://github.com/Molunerfinn/PicGo/releases)

gitee 的图床好像加了限制，七牛云域名免费一个月，最后还是选择了 github，虽然有时候会遇到图片打不开，但是最起码背靠大山。

# 一、创建 github 图床

## 1.1 创建仓库

首先仓库需要 public，其他的随便。

## 1.2 生成一个 token 用于 picgo 访问

点击头像 --> setting --> Developer settings --> Personal access tokens --> Tokens(classic) --> Generate new token(classic) --> Note 随便填，期限可以选一个月也可以无限，勾选 repo --> 创建
复制生成的 token，然后最好记下来，因为它只显示这一次。

# 二、配置 picGo

## 2.1 下载 picGo

[下载地址](https://github.com/Molunerfinn/PicGo/releases)

## 2.2 配置

![20240227104852](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/20240227104852.png)
图床配置名：随便填
设定仓库名：username/仓库名
设定分支名：main
设定 token：刚刚创建好的 token
设定存储路径：例如设置 img/后就会存入 img 文件夹中
设定自定义域名：https://cdn.jsdelivr.net/gh/username/仓库名@main；CDN加速域名。

此时配置完成，可以使用 picGo 上传图片。

# 三、配置 vscode

vscode 中安装扩展 picgo
![20240227105324](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/20240227105324.png)
左下角点击设置搜索 picgo
![20240227105402](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/20240227105402.png)
![20240227105522](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/20240227105522.png)
按照图片输入这些信息后就配置完成了。

# vscode 使用 picgo 方法

| 系统         |   剪贴板   | 资源管理器 |       截图 |
| :----------- | :--------: | :--------: | ---------: |
| Windows/unix | Ctrl+Alt+U | Ctrl+Alt+E | Ctrl+Alt+O |
| MacOs        | Cmd+Opt+U  | Cmd+Opt+E  |  Cmd+Opt+O |

例如使用微信截图后在 vscode 的 markdown 文件中直接使用 ctrl+alt+u 按键他就会自动把截图上传到 github 上。
