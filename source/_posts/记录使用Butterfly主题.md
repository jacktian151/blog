---
title: 记录使用hexo-theme-Butterfly主题
date: 2024/3/3 15:30:25 # 文章发表时间
tags:
  - hexo
categories: 博客 # 分类
cover: https://cdn.jsdelivr.net/gh/jacktian151/pic/记录使用Butterfly主题-2024-03-04-19-16-11.png # 略缩图
---

记录使用 hexo 主题：Butterfly。

# 安装主题

从 Butterfly 的 github clone 主题项目，在 hexo 的根目录操作：

```
git clone -b master https://gitee.com/immyw/hexo-theme-butterfly.git themes/butterfly
```

# 应用主题

修改 hexo 的\_config.yml 文件：

> theme: butterfly

# 安装插件

```
npm install hexo-renderer-pug hexo-renderer-stylus --save
npm install hexo-util --save
```

# 配置主题

## 修改主题配置文件

> 为了方便修改主题，在根目录创建\_config.butterfly.yml 文件，并把 themes 中的 butterfly 中的\_config.yml 文件中的内容复制到刚刚创建的文件中，Hexo 会自动合并主題中的 \_config.yml 和 \_config.butterfly.yml 里的配置，如果存在同名配置，会使用 \_config.butterfly.yml 的配置，其优先度高。 以后只需要在 \_config.butterfly.yml 进行配置就行。如果使用了 \_config.butterfly.yml， 配置主題的 \_config.yml 將不會有效果。

## 运行 hexo 博客

```npm
npm install
npm run server
```

在浏览器打开 localhost:4000 即可访问我们的博客了。

## 添加标签页

在 Hexo 博客的根目录输入

```
hexo new page tags
```

会自动创建：source/tags/index.md 文件，修改这个文件，添加 type: "tags"

```markdown
---
title: 标签
date: xxxxx
type: 'tags'
orderby: random
order: 1
---
```

## 添加分类页

在 Hexo 博客的根目录输入

```
hexo new page categories
```

会自动创建：source/categories/index.md 文件，修改这个文件，添加 type: "categories"

```markdown
---
title: 分类
date: 2024-03-03 11:15:15
type: 'categories'
---
```

## 导航栏设置

### 参数设置

在 \_config.butterfly.yml 中设置：

```yml
nav:
  logo: # 网站的 logo，支持图片，直接填入图片链接
  display_title: true # 是否显示网站标题，填写 true 或者 false
  fixed: false # 是否固定状态栏，填写 true 或者 false
```

### 菜单设置

```yml
Home: / || fas fa-home
Archives: /archives/ || fas fa-archive
Tags: /tags/ || fas fa-tags
Categories: /categories/ || fas fa-folder-open
List||fas fa-list:
  Music: /music/ || fas fa-music
  Movie: /movies/ || fas fa-video
Link: /link/ || fas fa-link
About: /about/ || fas fa-heart
```

必须是 /xxx/，后面||分开，然后写图标名。如果不希望显示图标，图标名可不写。默认子目录是展开的，如果你想要隐藏，在子目录里添加 hide 。

```
List||fas fa-list||hide:
  Music: /music/ || fas fa-music
  Movie: /movies/ || fas fa-video
```

## 图片设置

### 头像设置

图片存在 themes/butterfly/source/img/ 中

```yml
avatar:
  img: '/img/avatar.jpg'
  effect: false
```

### 博客顶部图片

```yml
# 顶部图是否展示：false：展示；true：不展示
disable_top_img: false

# 主页顶部图
index_img: '/img/bg.jpg'

# 默认的 top_img，当页面的 top_img 没有配置时，会显示 default_top_img
default_top_img: '/img/bg.jpg'

# 归档页面的 top_img
archive_img:

# tag 子页面 的 默认 top_img
tag_img:

# tag 子页面的 top_img，可配置每个 tag 的 top_img
# format:
#  - tag name: xxxxx
tag_per_img:

# If the banner of category page not setting, it will show the top_img
# category 子页面 的 默认 top_img
category_img:

# category 子页面的 top_img，可配置每个 category 的 top_img
# format:
#  - category name: xxxxx
category_per_img:
```

### 文章封面

```yml
# 文章封面
cover:
  # 是否显示文章封面
  index_enable: true
  aside_enable: true
  archives_enable: true
  # 方面显示的位置
  # left：左，right：右，both：左右轮流
  position: both
  # 当没有设置cover时，默认的封面显示，当配置多个时会随机选择一张
  default_cover:
    - https://i.loli.net/2020/05/01/gkihqEjXxJ5UZ1C.jpg
    - https://jsd.012700.xyz/gh/jerryc127/CDN@latest/cover/default_bg2.png
    - https://jsd.012700.xyz/gh/jerryc127/CDN@latest/cover/default_bg3.png
```

选择文章封面：
在每个文章中添加**cover:img 的 url**
例如：

```markdown
---
title: 记录gitlab部署hexo
date: 2024/2/7 16:30:25 # 文章发表时间
tags:
  - gitlab
  - hexo
categories: 博客 # 分类
cover: https://cdn.jsdelivr.net/gh/jacktian151/pic@main/gitlab+hexo-2024-02-27-16-18-41.png # 封面
---
```
