---
title: 记录gitlab部署hexo
date: 2024/2/7 16:30:25 # 文章发表时间
tags:
  - gitlab
  - hexo
categories: 博客 # 分类
cover: https://cdn.jsdelivr.net/gh/jacktian151/pic@main/gitlab+hexo-2024-02-27-16-18-41.png
---

- [一、前提：安装和配置环境](#一前提安装和配置环境)
  - [1. 安装 git](#1-安装-git)
  - [2. 安装 Node.js](#2-安装-nodejs)
- [二、新建 gitlab 仓库](#二新建-gitlab-仓库)
  - [1. 创建项目](#1-创建项目)
  - [2. 创建 Runner](#2-创建-runner)
    - [2.1 关闭 Runner](#21-关闭-runner)
    - [2.2 下载 Gitlab-Runner](#22-下载-gitlab-runner)
    - [2.3 安装 Runner 并启动](#23-安装-runner-并启动)
- [三、配置 hexo 博客](#三配置-hexo-博客)
  - [3.1 把创建好的空仓库 clone 下来，打开文件夹，在根目录创建.gitlab-ci.yml 文件](#31-把创建好的空仓库-clone-下来打开文件夹在根目录创建gitlab-ciyml-文件)
  - [3.2 本地全局安装 hexo-cli](#32-本地全局安装-hexo-cli)
  - [3.3 初始化 hexo 项目](#33-初始化-hexo-项目)
- [四、绑定腾讯云域名](#四绑定腾讯云域名)
  - [4.1 域名解析](#41-域名解析)
  - [4.2 gitlab 绑定域名](#42-gitlab-绑定域名)
  - [4.3 修改 Hexo 文件](#43-修改-hexo-文件)

> 记录一下使用[gitlab](https://gitlab.com/)部署[hexo](https://hexo.io/zh-cn/index.html)的全过程，好记性不如烂笔头。

# 一、前提：安装和配置环境

## 1. 安装 git

## 2. 安装 Node.js

# 二、新建 gitlab 仓库

国内用户可以使用[极狐 gitlab](https://jihulab.com/-/users/phone)

## 1. 创建项目

创建一个[username].gitlab.io 的项目，username 为你的用户名，也就是点击你的头像，下面的那个@xxx 就是你的用户名。选择公共的仓库，其他的随便。

## 2. 创建 Runner

### 2.1 关闭 Runner

gitlab 默认开启共享 Runner，我们这里选择自己创建，把默认开启的 Runner 关掉。
![20240227094257](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/20240227094257.png)

### 2.2 下载 Gitlab-Runner

[下载地址](https://docs.gitlab.com/runner/install/windows.html)，根据自己的电脑情况下载
![20240227094921](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/20240227094921.png)
下载完成后新建**D:\GitLab-Runner**文件夹，把下载下来的文件移入并改名:**gitlab-runner.exe**
打开 cmd 进入文件夹中运行：

```JavaScript
gitlab-runner.exe register
```

按提示步骤输入**url:https://gitlab.com/**和**token**
![20240227095238](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/20240227095238.png)
其他的直接回车，最后一项选择**shell**

### 2.3 安装 Runner 并启动

在**D:\GitLab-Runner**中启动管理员 cmd 并执行：

```JavaScript
gitlab-runner.exe install
gitlab-runner.exe start
```

此时在 gitlab 中的 runner 可以查看到刚刚新建的 runner 已经开始启动：
![20240227100217](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/20240227100217.png)

如果出现提交后失败，发现报错是什么“pwsh”环境错误之类的，这个是因为某些 win10 系统会报错，这是需要修改 D 盘的 gitlab-runner config.toml，把 shell = "pwsh"修改为 shell = "powershell"就可以了，然后

```
gitlab-runner.exe restart
```

重启 runner，就可以了。

# 三、配置 hexo 博客

## 3.1 把创建好的空仓库 clone 下来，打开文件夹，在根目录创建.gitlab-ci.yml 文件

```JavaScript
# 使用你的电脑安装的node版本
image: node:20.10.0-alpine
cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - npm install

pages:
  script:
    - npm run build
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

## 3.2 本地全局安装 hexo-cli

```JavaScript
npm install hexo-cli -g
```

## 3.3 初始化 hexo 项目

```JavaScript
hexo init blog
```

初始化后会出现一个**blog**的文件夹，把文件夹中的所有文件全部复制到从 gitlab 中 clone 下来的项目根目录中。
然后在项目目录下执行命令：

```JavaScript
npm install
npm run server
```

![20240227101535](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/20240227101535.png)
启动成功后打开 http://localhost:4000/就可以看到了。

启动成功后提交代码到 gitlab
![20240227101717](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/20240227101717.png)

可以看到出现这个标志就说明 gitlab 把博客自动部署了，此时打开浏览器输入**username.gitlab.io**就可以打开你的博客了。

# 四、绑定腾讯云域名

## 4.1 域名解析

在腾讯云控制台中打开我的域名，在右边点击解析：
![gitlab+hexo-2024-03-01-09-24-16](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/gitlab+hexo-2024-03-01-09-24-16.png)
点击添加记录，这里我们准备使用我们注册的域名的子域名来绑定我们的博客，我注册的域名为**sunsweet.top**，这里准备子域名为**myblog.sunsweet.top**来绑定我的博客。
![gitlab+hexo-2024-03-01-09-44-32](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/gitlab+hexo-2024-03-01-09-44-32.png)
解释一下：这里的主机记录就是我们子域名的一级域名，也就是 myblog，记录值为我们绑定的 gitlab 的 域名，如果想知道 ip 地址的话可以这样查看：
![gitlab+hexo-2024-03-01-09-28-55](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/gitlab+hexo-2024-03-01-09-28-55.png)
只需要 ping 一下 gitlab 的博客就可以查看到 IP 地址了。

## 4.2 gitlab 绑定域名

进入 gitlab 后打开我们的博客仓库，在 pages 中找到 Domains：
![gitlab+hexo-2024-03-01-09-33-17](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/gitlab+hexo-2024-03-01-09-33-17.png)
![gitlab+hexo-2024-03-01-09-33-36](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/gitlab+hexo-2024-03-01-09-33-36.png)
输入你的子域名后点击 Unverified 按钮重新验证后就可以了。
如果还不行的话就在域名解析中添加一个 TXT 解析：
![gitlab+hexo-2024-03-01-09-49-23](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/gitlab+hexo-2024-03-01-09-49-23.png)
记录值填写 gitlab 中的 code:
![gitlab+hexo-2024-03-01-09-50-32](https://cdn.jsdelivr.net/gh/jacktian151/pic@main/gitlab+hexo-2024-03-01-09-50-32.png)
添加后点击按钮验证通过。

## 4.3 修改 Hexo 文件

如果访问不正常的话还需要修改 hexo 的**\_config.yml**文件，把：

```
url: /
root: ""
```

的 url 修改成你的域名：

```
url: myblog.sunsweet.top
```

之后就可以正常使用了。
